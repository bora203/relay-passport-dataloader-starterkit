jest.unmock('lodash');
jest.unmock('../server/config/environment');
import config from '../server/config/environment';

describe('config.mysql', () => {
  it('should be defined', () => {
    expect(config).toBeDefined();
    expect(config.mysql).toBeDefined();
  });
});

jest.unmock('mysql');
jest.unmock('node-mysql');
jest.unmock('dataloader');
jest.unmock('cps');
jest.unmock('promise');
jest.unmock('../server/data/data-model');
jest.unmock('../server/data/mysql-dataloader');
import {User, UserLogin, Feature} from '../server/data/data-model';
import dataProvider from '../server/data/mysql-dataloader';

describe('dataProvider.addUser', () => {
    it('should be defined', () => {
        expect(dataProvider.addUser).toBeDefined();
    });

    it('should add a user to the db and return newly added user id', () => {
        dataProvider.addUser(new User(0, '', 'test@test.com', ''))
            .then((res) => {
                expect(res.id).toBeGreaterThan(0);
            })
            .catch((err) => {
                expect(err).toBeFalsy();
            });
    });
});
