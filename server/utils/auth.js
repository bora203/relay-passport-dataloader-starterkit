import db from '../data/database';
import expressJwt from 'express-jwt';
import jwt from 'jsonwebtoken';
import config from '../config/environment';

var jwtSecret = config.jwtSecret;

function generateToken(user) {
    return jwt.sign({ username: user.username }, new Buffer(config.jwtSecret, 'base64'));
}

function getTokenByUsernamePassword(username, password) {
    var user = db.getUserByUsername(username);
    var token;
    if(db.verifyPassword(user, password)) {
        token = generateToken(user);
    } else {
        token = null;
    }
    return token;
}

function getTokenByExternalLogin(loginProvider, login) {
    var user = db.getUserByExternalLogin(loginProvider, login);
    var token = generateToken(user);
    return token;
}

function tokenIssuer(req, res, next) {
    let username = req.params.username.toLowerCase( );
    let password = req.params.password;
    if(req.params.username && req.params.password) {
        try{
            var token = getTokenByUsernamePassword(req.params.username, req.params.password);
            if (token) {
                res.json({
                    success: true,
                    token: token 
                }); 
            } else {
                res.status( 401 ).json( { message: 'Incorrect password' } );
            }
        } catch (err) {
            res.status( 401 ).json( { message: err } );
        }
    }
}

function createUser(req, res, next) {
    let udata = {
        username: req.params.username.toLowerCase( ),
        password: req.params.password
    };
    db.addUser(udata).then( (user) => {
        var token = generateToken(user);
        res.json({
            success: true,
            token: token 
        }); 
    }).catch( (err) => {
        res.status( 401 ).json( { message: e } );
    });
}


function logout(req, res ) {
    res.cookie( 'UserToken1', '', { httpOnly: true, expires: new Date( 1 ) } );
    res.json( { success : true } );
}


// passport and passport-google-auth configuration
import passport from 'passport';
import passportGoogleOAuth from 'passport-google-oauth';
var GoogleStrategy = passportGoogleOAuth.OAuth2Strategy;

// Use the GoogleStrategy within Passport.
//   Strategies in Passport require a `verify` function, which accept
//   credentials (in this case, an accessToken, refreshToken, and Google
//   profile), and invoke a callback with a user object.
passport.use(new GoogleStrategy({
    clientID: config.googleAuth.GOOGLE_CLIENT_ID,
    clientSecret: config.googleAuth.GOOGLE_CLIENT_SECRET,
    callbackURL: "http://localhost:3000/auth/google/callback"
  },
  function(accessToken, refreshToken, profile, done) {
    db.
       User.findOrCreate({ googleId: profile.id }, function (err, user) {
         return done(err, user);
       });
  }
));

var auth = {
    authenticate: expressJwt({
        secret: new Buffer(config.jwtSecret, 'base64')
    }),

    setupAuthServer : function(appServer) {
        // POST /auth/createuser
        appServer.use('/auth/create_user', createUser);

        // POST /auth/token
        //   Issues a JWT token from username and password
        appServer.use('/auth/token', tokenIssuer);

        // POST /auth/logout
        appServer.use('/auth/logout', logout);

        // GET /auth/google
        //   Use passport.authenticate() as route middleware to authenticate the
        //   request.  The first step in Google authentication will involve
        //   redirecting the user to google.com.  After authorization, Google
        //   will redirect the user back to this application at /auth/google/callback
        appServer.use('/auth/google',
            passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/plus.login'] }));

        // GET /auth/google/callback
        //   Use passport.authenticate() as route middleware to authenticate the
        //   request.  If authentication fails, the user will be redirected back to the
        //   login page.  Otherwise, the primary route function function will be called,
        //   which, in this example, will redirect the user to the home page.
        appServer.use('/auth/google/callback', 
            passport.authenticate('google', { failureRedirect: '/login' }),
                function(req, res) {
                    res.redirect('/');
                });
    }
};

export default auth;
