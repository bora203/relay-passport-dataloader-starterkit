/* eslint-disable global-require */
import _ from 'lodash';

const config = {
  env: process.env.NODE_ENV || 'development',
  port: process.env.PORT || 3000,
  graphql: {
    port: 8000
  },
  mysql: {
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'sampledb'
  },
  jwtSecret: 'secret',
  googleAuth: {
    GOOGLE_CLIENT_ID: '264781885502-tkl6253kivo705uk1cqao88mgg3vklkf.apps.googleusercontent.com',
    GOOGLE_CLIENT_SECRET: 'iKJABaOA-huljYlwA7JYh4DE',
  }
};

export default _.extend(config, require(`./${config.env}`).default);
