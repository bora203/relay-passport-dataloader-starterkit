import dataProvider from './mysql-dataloader'
import {User, UserLogin, Feature} from './data-model';
import Promise from 'promise';
import bcrypt from 'bcrypt';

/**
 * 
 */
const features = [
  new Feature('1', 'React', 'A JavaScript library for building user interfaces.', 'https://facebook.github.io/react'),
  new Feature('2', 'Relay', 'A JavaScript framework for building data-driven react applications.', 'https://facebook.github.io/relay'),
  new Feature('3', 'GraphQL', 'A reference implementation of GraphQL for JavaScript.', 'http://graphql.org'),
  new Feature('4', 'Express', 'Fast, unopinionated, minimalist web framework for Node.js.', 'http://expressjs.com'),
  new Feature('5', 'Webpack', 'Webpack is a module bundler that packs modules for the browser.', 'https://webpack.github.io'),
  new Feature('6', 'Babel', 'Babel is a JavaScript compiler. Use next generation JavaScript, today.', 'https://babeljs.io'),
  new Feature('7', 'PostCSS', 'PostCSS. A tool for transforming CSS with JavaScript.', 'http://postcss.org'),
  new Feature('8', 'MDL', 'Material Design Lite lets you add a Material Design to your websites.', 'http://www.getmdl.io')
];

function passwordHash(plainPassword) {
  var saltRounds = 5;
  var hash = bcrypt.hashSync(user.password, saltRounds);
  return hash;
}

function verifyPassword(plainPassword, hashedPassword) {
  return bcrypt.compareSync(plainPassword, hashedPassword);
}

export default {
  User: User,
  UserLogin: UserLogin,
  Feature: Feature,

  getUserById: function(id, rootValue) {
    return dataProvider.userbyIdsLoader.load([id]) [0];
  },

  getUserByCredentials: function(creds) {
    var user = dataProvider.userbyUsernameLoader.load(creds.username);
    if(user && creds.password && user.password && verifyPassword(creds.password, user.password)) {
      return user;
    }
    return {};
  },

  getUserByExternalLogin: function(loginProvider, loginKey) {
    return dataProvider.userbyExternalLoginLoader.load(loginProvider, loginKey);
  },

  addUser: function(user) {
    //hashes user-entered plain-text password
    user.password = passwordHash(user.password);
    return dataProvider.addUser(user);
  },

  findOrCreateUserLogin: function(loginProvider, loginKey, profile) {
    return new Promise(function(fullfil, reject){
      var user = getUserByExternalLogin(loginProvider, loginKey);
      if(!user) {
        user = new User(0, '', loginKey, '');
        user.id = addUser(user);
        if(user.id) {
          dataProvider.addUserLogin(login);
          fullfil(user);
        } else {
          reject({
            message: 'Could not create a new User!'
          })
        }
      } else {
        fullfil(user);
      }
    });
  },

  getFeature: function(id) {
    return features.find(w => w.id === id);
  },

  getFeatures: function() {
    return features;
  }
};
