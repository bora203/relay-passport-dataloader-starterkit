class User {
  constructor(id, username, password, name, website) {
    this.id = id;
    this.name = name;
    this.username = username;
    this.password = password;
    this.website = website;
  }
}

class UserLogin {
  constructor(id, provider, provider_login, user_id) {
    this.id = id;
    this.provider = provider;
    this.provider_login = provider_login;
    this.user_id = user_id;
  }
}

class Feature {
  constructor(id, name, description, url) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.url = url;
  }
}

export {User, UserLogin, Feature};
