import db from 'node-mysql';
import DataLoader from 'dataloader';
import cps from 'cps';
import Promise from 'promise';
import config from '../config/environment';


/**
 * 
 */
var DB = db.DB;
var box = new DB(config.mysql);

// Parallelize all queries, but do not cache.
var queryLoader = new DataLoader(queries => new Promise(resolve => {
  var waitingOn = queries.length;
  var results = [];
  box.connect(function(conn, cb) {
    cps.peach(
      queries,
      function(q, cb) {
        conn.query(q, cb);
      },
      cb
    );
  }, function(res) {
    results = res;
    resolve(results);
  });
}), { cache: false });

// Dispatch a WHERE-IN query, ensuring response has rows in correct order.
var userbyExternalLoginLoader = new DataLoader((loginProvider, login) => {
  var query = `SELECT * FROM login_providers LEFT JOIN users ON users.id=login_providers.user_id WHERE provider=${loginProvider} AND provider_login=${login}`;
  return queryLoader.load([query]).then(
    row => row[0] || new Error(`Row not found`)
  );
});

var userbyUsernameLoader = new DataLoader(username => {
  var query = `SELECT * FROM users WHERE username=${username}`;
  return queryLoader.load([query]).then(
    row => row[0] || new Error(`Row not found`)
  );
});


var userbyIdsLoader = new DataLoader(ids => {
  var params = ids.map(id => '?' ).join();
  var query = `SELECT * FROM users WHERE id IN (${params})`;
  return queryLoader.load([query]).then(
    rows => ids.map(
      id => rows.find(row => row.id === id) || new Error(`Row not found: ${id}`)
    )
  );
});

// Dispatch a WHERE-IN query, ensuring response has rows in correct order.
var featureLoader = new DataLoader(ids => {
  var params = ids.map(id => '?' ).join();
  var query = `SELECT * FROM users WHERE id IN (${params})`;
  return queryLoader.load([query, ids]).then(
    rows => ids.map(
      id => rows.find(row => row.id === id) || new Error(`Row not found: ${id}`)
    )
  );
});

function addUser(user) {
  return new Promise(function (resolve, reject) {
    box.connect(function(conn, cb) {
      conn.query("INSERT INTO users(username, password) VALUES (?, ?)", [user.username, user.password], (err, result) => {
        if (err) throw err;
        console.log(result.insertId);
        cb(result);
      });
    }, res => {
      user.id = res.insertId;
      resolve(user);
    });
  });
}

function addUserLogin(login) {
  
}

function updateUser(user) {

}

export default {
  userbyIdsLoader: userbyIdsLoader,
  userbyUsernameLoader: userbyUsernameLoader,
  userbyExternalLoginLoader: userbyExternalLoginLoader,
  featureLoader: featureLoader,
  addUser: addUser
};
