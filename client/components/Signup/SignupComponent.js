import Relay from 'react-relay';
import React from 'react';
import { Grid, Cell, Textfield, Button, Spinner } from 'react-mdl';
import Page from '../Page/PageComponent';
import SignupMutation from '../../graphql/mutations/signup-mutation';

class Signup extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      loading: false
    };
    this.signup = this.signup.bind(this);
  }

  updateState(stateName) {
    var state = this.state;
    var refs = this.refs;
    return (e)=> {
      var newState={};
      newState[stateName] = e.target.value;  
      this.setState(newState);
    }
  };

  signup(e) {
    e.preventDefault();
    //this.setState({loading: true});
    var userdata = {
      username : this.state.username,
      password : this.state.password
    };
    /** Relay GraphQL based */
    var onSuccess = ({resp}) => {
      console.log('Mutation successful!');
      for (var key in localStorage) {
        localStorage.removeItem(key);
      }
      this.props.location.state ? this.props.history.pushState({}, this.props.location.state.previousPath) : this.props.history.goBack();
      //loginRequest(resp.user);
    };

    var onFailure = (transaction) => {
      var error = transaction.getError() || new Error('Mutation failed.');
      console.error(error);
    };

    if(this.props.user && this.props.user.user)
      console.log(this.props.user.user.username);
    else
      console.log(this.props.user);
    Relay.Store.commitUpdate( new SignupMutation({
        credentials: userdata,
        user: (this.props.user && this.props.user.user) ? this.props.user.user : userdata
      }),
      {onFailure, onSuccess});
  };

  render() {
    //const {user} = this.props.user;
    return (
      <Page heading='Signup'>
        <div style={{ width: '70%', margin: 'auto' }}>
          <div style={{display: this.state.loading?'block':'none', textAlign:'center'}}>
            <Spinner />
          </div>
          <Grid>
            <form style={{ margin: 'auto' }} onSubmit={this.signup}>
              <Cell col={12}>
                <Textfield onChange={this.updateState('username')} label='Username' required/>
              </Cell>
              <Cell col={12}>
                <Textfield type="password" onChange={this.updateState('password')} label='Password' required/>
              </Cell>
              <Cell col={12} style={{ textAlign: 'right' }}>
                <Button primary>Sign up</Button>
              </Cell>
            </form>
          </Grid>
        </div>
      </Page>
    );
  }
}


export default Relay.createContainer(Signup, {
  fragments: {
    user: () => Relay.QL`
      fragment on Query {
        user {
          username,
          ${SignupMutation.getFragment('user')}
        }
      }
      `
  }
});