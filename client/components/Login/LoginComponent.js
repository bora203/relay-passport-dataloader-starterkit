import Relay from 'react-relay';
import React from 'react';
import { Grid, Cell, Textfield, Button, Checkbox, Spinner } from 'react-mdl';
import Page from '../Page/PageComponent';
import LoginMutation from '../../graphql/mutations/login-mutation';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      loading: false
    };
    this.login = this.login.bind(this);
    this.googleAuth = this.googleAuth.bind(this);
  }

  updateState(stateName) {
    var state = this.state;
    var refs = this.refs;
    return (e)=> {
      var newState={};
      newState[stateName] = e.target.value;  
      this.setState(newState);
    }
  };

  login(e) {
    e.preventDefault();
    //this.setState({loading: true});
    var cred = {
      username : this.state.username,
      password : this.state.password
    };
    /** Relay GraphQL based */
    var onSuccess = ({resp}) => {
      for (var key in localStorage) {
        localStorage.removeItem(key);
      }
      this.props.location.state ? this.props.history.pushState({}, this.props.location.state.previousPath) : this.props.history.goBack();
      console.log('Mutation successful!');
      //loginRequest(resp.user);
    };
    var onFailure = (transaction) => {
      var error = transaction.getError() || new Error('Mutation failed.');
      console.error(error);
    };

    if(this.props.user && this.props.user.user)
      console.log(this.props.user.user.username);
    else
      console.log(this.props.user);
    Relay.Store.commitUpdate( new LoginMutation({
        credentials: cred,
        user: (this.props.user && this.props.user.user) ? this.props.user.user : cred 
      }), {onFailure, onSuccess});
  };

  googleAuth() {
    location.href = '/auth/google';
  }

  render() {
    //const {user} = this.props.user;
    return (
      <Page heading='Login'>
        <div style={{ width: '70%', margin: 'auto' }}>
          <div style={{display: this.state.loading?'block':'none', textAlign:'center'}}>
            <Spinner />
          </div>
          <Grid>
            <form style={{ margin: 'auto' }} onSubmit={this.login}>
              <Cell col={12}>
                <Textfield onChange={this.updateState('username')} label='Username' value={this.state.username} required/>
              </Cell>
              <Cell col={12}>
                <Textfield type="password" onChange={this.updateState('password')} label='Password' value={this.state.password} required/>
              </Cell>
              <Cell col={12}>
                <Checkbox label='Remember me' ripple style={{ textAlign: 'right' }} />
              </Cell>
              <Cell col={12} style={{ textAlign: 'right' }}>
                <a href='#'>Forgot password</a>
                <Button primary>Login</Button>
              </Cell>
            </form>
            <Cell col={12} style={{ textAlign: 'center' }}>
              <Button raised colored onClick={this.googleAuth}>Login with Google</Button>
            </Cell>
          </Grid>
        </div>
      </Page>
    );
  }
}

export default Relay.createContainer(Login, {
  fragments: {
    user: () => Relay.QL`
    fragment on Query {
      user {
        username,
        ${LoginMutation.getFragment('user')}
      }
    }
    `
  }
});