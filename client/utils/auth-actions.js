import fetch from 'isomorphic-fetch';

var authConfig = {
    tokenApiUrl: '/token',
    googleAuthUrl: '/auth/google'
};

function _setRelayAuthTokenHeader() {
  var token = localStorage.getItem('token');
  Relay.injectNetworkLayer(
    new Relay.DefaultNetworkLayer('http://localhost:3000/graphql', {
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
  );
}

// There are three possible states for our login
// process and we need actions for each of them
const LOGIN_REQUEST = 'LOGIN_REQUEST'
const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
const LOGIN_FAILURE = 'LOGIN_FAILURE'

function requestLogin(creds) {
  return {
    type: LOGIN_REQUEST,
    isFetching: true,
    isAuthenticated: false,
    creds
  };
}
function receiveLogin(user) {
  _setRelayAuthTokenHeader();
  return {
    type: LOGIN_SUCCESS,
    isFetching: false,
    isAuthenticated: true,
    token: user.token
  };
}
function loginError(message) {
  return {
    type: LOGIN_FAILURE,
    isFetching: false,
    isAuthenticated: false,
    message
  };
}

// Calls the API to get a token and
// dispatches actions along the way
function loginUser(creds) {
  let config = {
    method: 'POST',
    headers: { 'Content-Type':'application/x-www-form-urlencoded' },
    body: `username=${creds.username}&password=${creds.password}`
  };

  return dispatch => {
    // We dispatch requestLogin to kickoff the call to the API
    dispatch(requestLogin(creds));
    
    return fetch('http://localhost:3000/token', config)
      .then( response =>response.json().then(user => ({ user, response })) )
      .then(({ user, response }) =>  {
        if (!response.ok) {
          // If there was a problem, we want to
          // dispatch the error condition
          dispatch(loginError(user.message));
          return Promise.reject(user);
        } else {
          // If login was successful, set the token in local storage
          localStorage.setItem('token', user.token);
          // Dispatch the success action
          dispatch(receiveLogin(user));
        }
      }).catch(err => console.log("Error: ", err));
  };
}


// Three possible states for our signup process as well.
// Since we are using JWTs, we just need to remove the token
// from localStorage. These actions are more useful if we
// were calling the API to log the user out
const SIGNUP_REQUEST = 'SIGNUP_REQUEST'
const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS'
const SIGNUP_FAILURE = 'SIGNUP_FAILURE'

function requestSignup(udata) {
  return {
    type: SIGNUP_REQUEST,
    isFetching: true,
    isAuthenticated: false,
    creds
  };
}
function receiveSignup(user) {
  _setRelayAuthTokenHeader();
  return {
    type: SIGNUP_SUCCESS,
    isFetching: false,
    isAuthenticated: true,
    token: user.token
  };
}
function signupError(message) {
  return {
    type: SIGNUP_FAILURE,
    isFetching: false,
    isAuthenticated: false,
    message
  };
}
// Calls the API to get a token and
// dispatches actions along the way
function signupUser(udata) {

  let config = {
    method: 'POST',
    headers: { 'Content-Type':'application/x-www-form-urlencoded' },
    body: `username=${udata.username}&password=${udata.password}`
  };

  return dispatch => {
    // We dispatch requestSignup to kickoff the call to the API
    dispatch(requestSignup(udata));
    
    return fetch('http://localhost:3000/create_user', config)
      .then( response =>response.json().then(user => ({ user, response })) )
      .then(({ user, response }) =>  {
        if (!response.ok) {
          // If there was a problem, we want to
          // dispatch the error condition
          dispatch(signupError(user.message));
          return Promise.reject(user);
        } else {
          // If signup was successful, set the token in local storage
          localStorage.setItem('token', user.token);
          // Dispatch the success action
          dispatch(receiveSignup(user));
        }
      }).catch(err => console.log("Error: ", err));
  };
}

// Three possible states for our logout process as well.
// Since we are using JWTs, we just need to remove the token
// from localStorage. These actions are more useful if we
// were calling the API to log the user out
const LOGOUT_REQUEST = 'LOGOUT_REQUEST'
const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS'
const LOGOUT_FAILURE = 'LOGOUT_FAILURE'

function requestLogout() {
  return {
    type: LOGOUT_REQUEST,
    isFetching: true,
    isAuthenticated: true
  }
}
function receiveLogout() {
  return {
    type: LOGOUT_SUCCESS,
    isFetching: false,
    isAuthenticated: false
  }
}
// Logs the user out
function logoutUser() {
  return dispatch => {
    dispatch(requestLogout())
    localStorage.removeItem('token')
    dispatch(receiveLogout())
  } 
}

// google auth action
function googleAuth() {
  location.href = '/auth/google';
}

export default {
  LOGIN_REQUEST : LOGIN_REQUEST,
  LOGIN_SUCCESS : LOGIN_SUCCESS,
  LOGIN_FAILURE : LOGIN_FAILURE,
  LOGOUT_REQUEST : LOGOUT_REQUEST,
  LOGOUT_SUCCESS : LOGOUT_SUCCESS,
  LOGOUT_FAILURE : LOGOUT_FAILURE,
  SIGNUP_REQUEST : SIGNUP_REQUEST,
  SIGNUP_SUCCESS : SIGNUP_SUCCESS,
  SIGNUP_FAILURE : SIGNUP_FAILURE,
  loginUser: loginUser,
  signupUser: signupUser,
  logoutUser: logoutUser,
  googleAuth: googleAuth
}
