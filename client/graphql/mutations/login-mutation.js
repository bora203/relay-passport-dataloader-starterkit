import Relay from 'react-relay';

export default class LoginMutation extends Relay.Mutation {
  static fragments = {
    user: () => Relay.QL`
      fragment on User {
        username
      }
    `,
  };
  
  getMutation() {
    return Relay.QL`mutation{Login}`;
  }

  getVariables() {
    return {
      username: this.props.credentials.username,
      password: this.props.credentials.password,
    };
  }
  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        user: this.props.user.username,
      }
    }];
  }
  getOptimisticResponse() {
    return {
      username: this.props.credentials.username,
    };
  }
  getFatQuery() {
    return Relay.QL`
    fragment on LoginPayload {
      user {
        username
      }
    }
    `;
  }
}